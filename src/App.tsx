import React from 'react';
import './App.css';
import Board from './component/Board';

export class App extends React.Component {
    render(): JSX.Element {
        return (
            <div className="App">
                <Board/>
            </div>
        );
    }
}

export default App;
