import React from 'react';
import './NumberButton.css';

export interface NumberButtonProps {
    className: string;
    value: string;
    color: string;
    onClick: (x: React.MouseEvent<HTMLElement>) => void;
}

export interface NumberButtonState {
    btnBackground: string;
}

export class NumberButton extends React.Component<NumberButtonProps, NumberButtonState> {
    constructor(props: NumberButtonProps) {
        super(props);
        this.state = {
            btnBackground: this.props.color
        };
    }

    mouseDown(): void {
        this.setState({
                btnBackground: 'white'
            }
        );
    }

    mouseUp(): void {
        this.setState({
            btnBackground: this.props.color
        });
    }

    render(): JSX.Element {
        return (
            <div
                className={'calcButton ' + this.props.className}
                style={{backgroundColor: this.state.btnBackground}}
                onClick={this.props.onClick}
                onMouseDown={this.mouseDown.bind(this)}
                onMouseUp={this.mouseUp.bind(this)}
            >
                {this.props.value}
            </div>
        );
    }
}

export default NumberButton;
