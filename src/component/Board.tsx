import React from 'react';
import './Board.css';
import NumberButton from './NumberButton';

export interface BoardProps {
    onClick?: (x: string) => void;
}

export interface BoardState {
    displayedValue: string;
    resultValue: string;
    operator: string;
    mathResult: string;
    finalValue: string;
}

export class Board extends React.Component<BoardProps, BoardState> {
    constructor(props: BoardProps) {
        super(props);
        this.state = {
            displayedValue: '',
            resultValue: '0',
            operator: '',
            mathResult: '0',
            finalValue: '0'
        };
    }

    doMathOperation(x: string, y: string, anMathAction: string): string {
        switch (anMathAction) {
            case '+':
                return (parseFloat(x) + parseFloat(y)).toString();
            case '-':
                return (parseFloat(x) - parseFloat(y)).toString();
            case 'x':
                return (parseFloat(x) * parseFloat(y)).toString();
            case '/':
                return (parseFloat(x) / parseFloat(y)).toString();
            default:
                return '';
        }
    }

    isAction(value: string): boolean {
        return value === '+' || value === '-' || value === 'x' || value === '/' || value === '=';
    }

    handleClick(val: React.MouseEvent<HTMLElement>): void {
        const display: string = this.state.displayedValue;
        const result: string = this.state.resultValue;
        const mathOperator: string = this.state.operator;
        const mathReesult: string = this.doMathOperation(result, display || '0', mathOperator);
        if (val.currentTarget.innerText === 'clear') {
            this.setState({
                displayedValue: '',
                resultValue: '0',
                operator: '',
                mathResult: '0',
                finalValue: '0'
            });
        } else if (val.currentTarget.innerText === '=') {
            this.setState({
                finalValue: mathReesult
            });
        } else if (!this.isAction(val.currentTarget.innerHTML)) {
            this.setState({
                displayedValue: display + val.currentTarget.innerHTML,
                finalValue: display + val.currentTarget.innerHTML
            });
        } else if (!mathOperator) {
            this.setState({
                resultValue: this.state.displayedValue,
                displayedValue: '',
                operator: val.currentTarget.innerHTML,
                mathResult: mathReesult,
                finalValue: this.state.displayedValue
            });
        } else {
            this.setState({
                resultValue: mathReesult,
                displayedValue: '',
                mathResult: mathReesult,
                operator: val.currentTarget.innerHTML,
                finalValue: mathReesult
            });
        }
    }

    render(): JSX.Element {
        return (
            <div className="div-center noselect">
                <table>
                    <tbody>
                    <tr>
                        <td colSpan={4} className="display-area">{this.state.finalValue.substring(0, 12)}</td>
                    </tr>
                    <tr>
                        <td colSpan={3}>
                            <NumberButton
                                className="clear"
                                color="darkgray"
                                value="clear"
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                        <td>
                            <NumberButton
                                className="number"
                                color="darkorange"
                                value="/"
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <NumberButton
                                className="number"
                                color="gray"
                                value="7"
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                        <td>
                            <NumberButton
                                className="number"
                                color="gray"
                                value="8"
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                        <td>
                            <NumberButton
                                className="number"
                                color="gray"
                                value="9"
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                        <td>
                            <NumberButton
                                className="number"
                                color="darkorange"
                                value="x"
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <NumberButton
                                className="number"
                                color="gray"
                                value="4"
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                        <td>
                            <NumberButton
                                className="number"
                                color="gray"
                                value="5"
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                        <td>
                            <NumberButton
                                className="number"
                                color="gray"
                                value="6"
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                        <td>
                            <NumberButton
                                className="number"
                                color="darkorange"
                                value="-"
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <NumberButton
                                className="number"
                                color="gray"
                                value="1"
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                        <td>
                            <NumberButton
                                className="number"
                                color="gray"
                                value="2"
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                        <td>
                            <NumberButton
                                className="number"
                                color="gray"
                                value="3"
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                        <td>
                            <NumberButton
                                className="number"
                                color="darkorange"
                                value="+"
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                    </tr>
                    <tr>
                        <td colSpan={2}>
                            <NumberButton
                                className="doubleNumber"
                                color="gray"
                                value="0"
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                        <td>
                            <NumberButton
                                className="number"
                                color="gray"
                                value="."
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                        <td className="action-button">
                            <NumberButton
                                className="number"
                                color="darkorange"
                                value="="
                                onClick={this.handleClick.bind(this)}
                            />
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Board;
